﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.ASP.Models.Forms
{
    public class BmiForm
    {
        [Required(ErrorMessage = "Ce champs est requis")]
        [Range(0, 300, ErrorMessage = "Taille non valide")]
        public int Taille { get; set; } // cm

        [Required]
        [Range(0, 1000)]
        public int Poids { get; set; } // kg

        public float Bmi 
        { 
            get { return Poids / (Taille * Taille / 10000f); } 
        }
    }
}
