﻿using Demo.ASP.Models.Forms;
using Demo.ASP.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Demo.ASP.Controllers
{
    public class ExerciceController : Controller
    {
        public IActionResult Bmi()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Bmi([FromForm] BmiForm form)
        {
            if(ModelState.IsValid) // vérifier la validité du forumulaire
            {
                // faire action ...
                return View(form);
            }
            return View();
        }

        public IActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Contact([FromForm] ContactForm form)
        {
            if(ModelState.IsValid)
            {
                // envoyer un email

                if(!(new MailService()).SendEmail(form.Subject, form.Content, "lykhun@gmail.com"))
                {
                    TempData["error_message"] = "Une erreur est survenue";
                    // rester sur la page
                    return View(form);
                }

                // afficher une message
                TempData["success_message"] = "Le message a bien été envoyé";
                // rediriger autre part
                return RedirectToAction("Index", "Home");
            }
            // afficher un message d'erreur
            TempData["error_message"] = "Une erreur est survenue";
            // rester sur la page
            return View(form);
        }
    }
}
