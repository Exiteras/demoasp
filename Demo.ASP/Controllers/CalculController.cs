﻿using Demo.ASP.Models.Forms;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.ASP.Controllers
{
    public class CalculController : Controller
    {
        // affichage du formulaire
        public IActionResult Formulaire()
        {
            return View();
        }

        // traitement du forulaire
        [HttpPost]
        public IActionResult Formulaire([FromForm]TableForm body)
        {
            return View(body);
        }
    }
}
