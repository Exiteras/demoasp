﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Threading.Tasks;

namespace Demo.ASP.Services
{
    public class MailService
    {
        /// <summary>
        /// Envoyer un email
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="content"></param>
        /// <param name="dests"></param>
        /// <returns></returns>
        public bool SendEmail(string subject, string content, params string[] dests)
        {
            using (SmtpClient client = new SmtpClient())
            {
                client.Host = "smtp.gmail.com";
                client.Port = 587;
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential("tf2021ASPDEMO@gmail.com", "technofutur");
                using (MailMessage message = new MailMessage())
                {
                    message.From = new MailAddress("tf2021ASPDEMO@gmail.com");
                    message.Subject = subject;
                    message.Body = content;
                    message.IsBodyHtml = true;
                    foreach (string dest in dests)
                    {
                        message.To.Add(dest);
                    }
                    try
                    {
                        client.Send(message);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }
            }
        }
    }
}
